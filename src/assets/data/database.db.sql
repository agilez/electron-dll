BEGIN TRANSACTION;
DROP TABLE IF EXISTS `products`;
CREATE TABLE IF NOT EXISTS `products` (
	`id`	INTEGER NOT NULL,
	`name`	TEXT NOT NULL,
	`barcode` TEXT,
	`ncm` TEXT,
	`cfop` TEXT,
	`measurement_id` TEXT,
	`cst_a_id` TEXT,
	`cst_b_id` TEXT,
	`cofins_id` TEXT,
	`pis_id` TEXT,
	`csosn_id` TEXT,
	`origin_id` TEXT,
	`photo_url` TEXT,
	`amount` REAL,
	`available` INTEGER,
	`sales_count` INTEGER
);
DROP TABLE IF EXISTS `services`;
CREATE TABLE IF NOT EXISTS `services` (
	`id`	INTEGER NOT NULL,
	`name`	TEXT NOT NULL,
	`amount` REAL,
	`sales_count` INTEGER,
	`courtesy_id` INTEGER,
	`photo_url` TEXT
);
DROP TABLE IF EXISTS `materials`;
CREATE TABLE IF NOT EXISTS `materials` (
	`id`	INTEGER,
	`name`	TEXT,
	`available` INTEGER,
	`minimum_quantity` INTEGER
);
DROP TABLE IF EXISTS `adjusts`;
CREATE TABLE IF NOT EXISTS `adjusts` (
	`reason`	TEXT,
	`operation`	TEXT,
	`item_type`	TEXT,
	`item_id`	INTEGER,
	`quantity` INTEGER,
	`created_by_token` TEXT,
	`synced` BOOLEAN,
	`created_at` TEXT DEFAULT (datetime('now','localtime'))
);
DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
	`id`	INTEGER,
	`name`	TEXT,
	`email`	TEXT,
	`username`	TEXT,
	`avatar_url`	TEXT,
	`profile`	TEXT,
	`auth_token`	TEXT
);
DROP TABLE IF EXISTS `payment_methods`;
CREATE TABLE IF NOT EXISTS `payment_methods` (
	`id`	INTEGER NOT NULL,
	`name`	TEXT NOT NULL
);
DROP TABLE IF EXISTS `categories`;
CREATE TABLE IF NOT EXISTS `categories` (
	`id`	INTEGER NOT NULL,
	`name`	TEXT NOT NULL
);
DROP TABLE IF EXISTS `sales`;
CREATE TABLE IF NOT EXISTS `sales` (
	`id` INTEGER,
	`customer_id`	INTEGER,
	`customer_app_id`	INTEGER,
	`customer_name`	TEXT,
	`customer_federal_tax_id`	TEXT,
	`customer_email`	TEXT,
	`customer_phone`	TEXT,
	`charged_name`	TEXT,
	`charged_token`	TEXT,
	`professional_id`	INTEGER,
	`professional_name`	TEXT,
	`cashier_id`	INTEGER,
	`subtotal` REAL,
	`discount` REAL,
	`total` REAL,
	`reset_design` INTEGER,
	`status` TEXT,
	`invoice_number` TEXT,
	`invoice_serie` TEXT,
	`access_key` TEXT,
	`authorization_xml` TEXT,
	`emission_at` TEXT,
	`emission_by_token` TEXT,
	`pending_issue` BOOLEAN,
	`reason` TEXT,
	`pending_cancel` BOOLEAN,
	`cancel_xml` TEXT,
	`canceled_at` TEXT,
	`canceled_by_token` TEXT,
	`synced` BOOLEAN,
	`created_at` TEXT 
);
DROP TABLE IF EXISTS `entries`;
CREATE TABLE IF NOT EXISTS `entries` (
	`id`	INTEGER,
	`operation`	INTEGER,
	`cashier_id`	INTEGER,
	`relatable_type`	INTEGER,
	`category_id`	INTEGER,
	`relatable_id`	INTEGER,
	`relatable_rowid`	INTEGER,
	`payment_method_id`	INTEGER,
	`amount` REAL,
	`created_by_token`	TEXT,
	`created_by_name`	TEXT,
	`created_at` TEXT DEFAULT (datetime('now','localtime')),
	`status` TEXT,
	`synced` BOOLEAN,
	`deleted_at` TEXT,
	`deleted_by_token` TEXT,
	`note` TEXT
);
DROP TABLE IF EXISTS `sale_items`;
CREATE TABLE IF NOT EXISTS `sale_items` (
	`sale_id`	INTEGER,
	`used_points`	INTEGER,
	`item_type`	TEXT,
	`item_id`	TEXT,
	`item_name`	TEXT,
	`quantity`	TEXT,
	`amount` REAL,
	`discount` REAL,
	`discount_reason` TEXT
);
DROP TABLE IF EXISTS `sale_payments`;
CREATE TABLE IF NOT EXISTS `sale_payments` (
	`sale_id`	INTEGER,
	`payment_method_id`	INTEGER,
	`payment_method_name`	TEXT,
	`parcels` INTEGER,
	`credit_card_flag_id` INTEGER,
	`amount` REAL
);
DROP TABLE IF EXISTS `appointments`;
CREATE TABLE IF NOT EXISTS `appointments` (
	`id`	INTEGER,
	`professional_id`	INTEGER,
	`customer_id` INTEGER,
	`customer_app_id`	INTEGER,
	`customer_name` TEXT,
	`customer_federal_tax_id` TEXT,
	`customer_email`	TEXT,
	`customer_phone`	TEXT,
	`service_id` INTEGER,
	`note` TEXT,
	`started_at` TEXT,
	`ended_at` TEXT,
	`created_at` TEXT,
	`deleted_at` TEXT,
	`synced` BOOLEAN,
	`status` TEXT,
	`created_by_token`	TEXT,
	`updated_by_token`	TEXT
);
COMMIT;

