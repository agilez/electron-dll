var ffi = require('ffi-napi');
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html'
})
export class HomeComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
    new Promise<any>((resolve, reject) => {
      this.loadLibs(0).then((success) => {
        if(success){
          var dll = ffi.Library('E1_Impressora.dll',  {
            'AbreConexaoImpressora': [ 'int', [ 'int','string','string', 'int' ]],
            'Corte': [ 'int', [ 'int' ]],
            'ImprimeXMLSAT': [ 'int', ['string', 'int']],
            'ImprimeXMLNFCe': [ 'int', ['string', 'string', 'string', 'int']],
            'StatusImpressora': ['int', [ 'int' ]],
            'FechaConexaoImpressora': ['int', []]
          });
          console.log('loading E1_Impressora.dll > ', dll);
        } else{
          alert('Falha ao se conectar com a impressora Elgin. Cód. I01');
          reject();
        } 
      });
    });
  }

  public loadLibs(index:number):Promise<boolean>{
    return new Promise<boolean>((resolve) => {
      if(index < 8){
        const lib_names = ['libwinpthread-1.dll','libstdc++-6.dll','libgcc_s_dw2-1.dll','Qt5Core.dll','Qt5Network.dll','Qt5SerialPort.dll','Qt5Xml.dll','Qt5XmlPatterns.dll'];
        const lib = ffi.Library(lib_names[index], {});
        console.log('loading ' + lib_names[index] + ' > ', lib);
        if(lib){
          this.loadLibs(index + 1).then((result)=>{
            resolve(result);
          })
        } else {
          resolve(false);
        }
      }else{
        resolve(true);
      }
    });
  }

}
