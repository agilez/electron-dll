var path = require('path');
var webpack = require('webpack');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = {
    entry: {
        'polyfills': './src/polyfills.ts',
        'vendor': './src/vendor.ts',
        'app': './src/main.ts'
    },

    resolve: {
        extensions: ['.ts', '.js', '.node']
    },

    externals: {
        sqlite3: 'commonjs sqlite3',
    },

    module: {
        rules: [
            {
                test: /\.ts$/,
                exclude: /\.spec\.ts$/,
                use: ['awesome-typescript-loader', 'angular2-template-loader']
            },
            {
                test: /\.html$/,
                use: [
                    {
                        loader: 'html-loader'
                    },
                ]
            },
            {
                test: /\.(png|jpe?g|gif|svg|bmp|woff|woff2|ttf|eot|ico)$/,
                use: 'file-loader?name=assets/images/[name].[ext]'
            },
            {
                test: /\.(eot|ttf|woff|woff2)$/,
                use: 'file-loader?name=assets/fonts/[name].[ext]'
            },
            {
                test: /\.(.dll|.ini|.chm)$/,
                use: 'file-loader?name=./[name].[ext]'
            },
			{
				test: /\.scss$/,
                use: [
                    {
                        loader: "to-string-loader"
                    },
                    {
                        loader: "style-loader"
                    },
                    {
                        loader: 'postcss-loader', // Run post css actions
                        options: {
                            postcssOptions: {
                                plugins: function () { // post css plugins, can be exported to postcss.config.js
                                    return [
                                        require('precss'),
                                        require('autoprefixer')
                                    ];
                                }
                            }
                        }
                    },
                    {
                        loader: "sass-loader",
                        options: {
                            sassOptions: {
                                includePaths: [
                                    path.resolve(__dirname, '../src/assets/sass'),
                                    path.resolve(__dirname, '../node_modules/bootstrap/scss'),
                                ]
                            }
                        }
                    }
                ]
            },
            {
                test: /\.node$/,
                use: 'node-loader'
            }
        ]
    },

    plugins: [
        new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery',
            'window.jQuery': 'jquery',
            Popper: ['popper.js', 'default'],
        }),
        new HtmlWebpackPlugin({
            baseUrl: process.env.NODE_ENV == 'development'?'/':'/app/',
            template: 'src/index.html'
        }),
		new CopyWebpackPlugin(
            {
                patterns: [
                    { from: 'src/assets/data/database\.*', to: 'assets/data/[name][ext]' },
                    { from: 'src/assets/fonts/*\.*', to: 'assets/fonts/[name][ext]' },
                    { from: 'src/assets/i18n/*\.*', to: 'assets/i18n/[name][ext]' },
                    { from: 'dlls/*\.*', to: './[name][ext]' },
                    { from: 'node_modules/ffi-napi/build/Release/ffi_bindings.node', to: 'app/build/[name][ext]' },
                ]
            }
        ),
        new webpack.ContextReplacementPlugin(
            /angular(\\|\/)core(\\|\/)(@angular|esm5)/,
            path.resolve(__dirname, '../src')
        ),
        new webpack.NormalModuleReplacementPlugin(
            /^bindings$/,
            require.resolve("./bindings")
        )
    ],

    target:'electron-renderer'
};
