var webpackMerge = require('webpack-merge').merge;
var commonConfig = require('./webpack.common.js');

module.exports = webpackMerge(commonConfig, {
    mode: 'development',
    devtool: 'inline-source-map',
});
