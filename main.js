const electron = require('electron');
const { app, BrowserWindow, ipcMain, shell } = electron;
// const { autoUpdater } = require("electron-updater");
const path = require('path');
const devMode = /electron/.test(path.basename(app.getPath('exe'), '.exe')) || process.platform == 'darwin';

if (devMode) {
	app.setName(app.getName());
	app.setPath('userData', app.getPath('userData'));
	require('electron-reload')(path.join(__dirname, '/dist/app.js'), {
		electron: path.join(__dirname, 'node_modules', '.bin', 'electron')
	});
}

let win;
let createWindow = () => {
	//const { width, height } = electron.screen.getPrimaryDisplay().workAreaSize;
	win = new BrowserWindow({
		show: true,
		frame: false,
		resizable: true,
		movable: true,
		webPreferences: {
            nodeIntegration: true,
            contextIsolation: false,
        },
		width: 800,
		height: 600
	});
	win.setMenu(null);
	win.once('ready-to-show', () => {
		//win.show()
	});
	ipcMain.on('fullscreen', () => {
		win.setFullScreen(true);
	})
	ipcMain.on('unmaximize', () => {
		win.setFullScreen(false);
	})
	win.loadURL('file://' + __dirname + '/dist/index.html');
	if (devMode)
		win.webContents.openDevTools();
	ipcMain.on('close', () => {
		win.close();
	})
	win.on('closed', () => {
		win = null;
	});
}

const goTheLock = app.requestSingleInstanceLock();

if (!goTheLock) {
  app.quit();
  return;
}else{
	app.on('second-instance', function(event, commandLine, workingDirectory) {
		if (win) {
			if (win.isMinimized()) win.restore();
			win.focus();
		}
	});
}

app.on('ready', createWindow);

/* 
app.on('window-all-closed', () => {
	if (process.platform !== 'darwin') {
		app.quit();
	}
});

app.on('activate', () => {
	if (win === null) createWindow();
});

ipcMain.on('getVersion', () => {
	win.webContents.send('version',  {version: app.getVersion()});
});

ipcMain.on('print-nfe', (event, sale) => {
	winPrint = new BrowserWindow({
		show: true,
		frame: false,
		resizable: false,
		movable: true,
		width: 320,
		height: 700
	});
	winPrint.setMenu(null);
	winPrint.loadURL('file://' + __dirname + '/pos.html');
	ipcMain.once('print-ready', () => {
		winPrint.webContents.send('sale-data', {sale: sale});
	});
})

ipcMain.on('checkUpdate', () => {
	autoUpdater.autoDownload = false;
	autoUpdater.once('update-available', () => win.webContents.send('updateStatus', { status: 2 } ))
	autoUpdater.once('error', () => win.webContents.send('updateStatus', {status: -1 } ))
	autoUpdater.once('update-not-available', () => win.webContents.send('updateStatus', {status: 1} ))
	autoUpdater.checkForUpdates().then(()=> {
		console.log('checado');
	}).catch(() => {
		win.webContents.send('updateStatus', {status: 1});
	});
});

ipcMain.once('downloadUpdate', () => {
	autoUpdater.once('update-downloaded', (ev, info) => { 
		win.webContents.send('updateStatus', {status: 4})
		setTimeout(()=> autoUpdater.quitAndInstall(), 3000);
	});
	autoUpdater.downloadUpdate().then(()=> {
		console.log('downloadUpdate')
	})
})
ipcMain.on('minimize', () => {
	win.minimize();
})

ipcMain.on('wa', () => {
	shell.openExternal('https://wa.me/551241040023');
})
ipcMain.on('help', () => {
	shell.openExternal('http://ajuda.contotal.com.br');
})

ipcMain.on('devtools', () => {
	win.webContents.toggleDevTools();
});
*/